import { _decorator, CCInteger, Component, director, EventKeyboard, Input, input, KeyCode, Node, Collider2D, IPhysics2DContact, Contact2DType, Sprite, color, Color } from 'cc';
const { ccclass, property } = _decorator;
import { Ground } from './Ground';
import { Results } from './Results';
import { Bird } from './Bird';
import { PipePool } from './PipePool';
import { BirdAudio } from './BirdAudio';

@ccclass('GameManager')
export class GameManager extends Component {
    
    @property({
        type:Ground,
        tooltip:'this is ground'
    })
    public ground: Ground;

    @property({
        type: Results,
        tooltip: 'result go here'
    })
    public result: Results;

    @property({
        type:Bird,
        tooltip: 'this is bird'
    })
    public bird: Bird;

    @property({
        type:PipePool,
        tooltip:'this is pipePool'
    })
    public pipeQueue: PipePool;

    @property({
        type:BirdAudio,
        tooltip:'this is audio'
    })
    public clip: BirdAudio;

    @property({
        type:CCInteger,
    })
    public speed:number = 300;
    
    @property({
        type:CCInteger,
    })
    public pipeSpeed:number = 200;

    public isOver:boolean;

    @property({
        type:[Node]
    })
    public skin:Node[] = [];

    @property({
        type:Sprite
    })
    sprite: Sprite = null;

    private interval: number = 10;
    private duration: number = 3;
    private targetOpacity: number = 0;

    private isIncreasingOpacity: boolean = false;


    onLoad(): void {
        this.initListener();
        this.result.resetScore();
        this.isOver = true;

        let getSkin = parseInt(localStorage.getItem("Skin"));

        if(getSkin === 0 ){
            this.skin[0].active = true;
            this.skin[1].active = false;
            this.skin[2].active = false;
        } else if(getSkin === 1 ){
            this.skin[0].active = false;
            this.skin[1].active = true;
            this.skin[2].active = false;
        }else if(getSkin === 2 ){
            this.skin[0].active = false;
            this.skin[1].active = false;
            this.skin[2].active = true;
        }
        
        this.schedule(this.changeTransparency, this.interval);

        director.pause();
    }

    initListener(){

       // input.on(Input.EventType.KEY_DOWN, this.onKeyDown, this);

        this.node.on(Node.EventType.TOUCH_START, () => {

            if(this.isOver == true){
                this.resetGame();
                this.bird.resetBird();
                this.startGame();
            }
            if(this.isOver == false){
                this.bird.fly();
                this.clip.onAudioQueue(0);
            }
        })
    }

    onKeyDown(event: EventKeyboard){
        switch(event.keyCode)
        {
            case KeyCode.KEY_A:
                this.gameOver();
            break;

            case KeyCode.KEY_P:
                this.result.addScore();
            break;

            case KeyCode.KEY_Q:
                this.resetGame();
                this.bird.resetBird();
        }
            

    }

    startGame(){
        this.result.hideResults();
        director.resume();
    }

    gameOver(){
        this.result.showResults();
        this.isOver = true;
        this.clip.onAudioQueue(3);
        director.pause();
    }

    resetGame(){
        this.result.resetScore();
        this.pipeQueue.reset();
        this.isOver = false;
        this.startGame();
    }

    passPipe(){
        this.result.addScore();
        this.clip.onAudioQueue(1);
    }

    createPipe(){
        this.pipeQueue.addPool();
    }

    contactGroundPipe(){
        let collider = this.bird.getComponent(Collider2D);

        if(collider){
            collider.on(Contact2DType.BEGIN_CONTACT, this.onBeginContact, this)
        }
    }

    onBeginContact(seftCollider: Collider2D, otherCollider: Collider2D, contact: IPhysics2DContact | null){
        this.bird.hitSomething = true;
        this.clip.onAudioQueue(2);
    }

    birdStruck(){
        this.contactGroundPipe();

        if(this.bird.hitSomething == true){
            this.gameOver();
        }
    }

    update(){
        if(this.isOver == false){
            this.birdStruck();
        }
    }

    changeTransparency() {
        if (this.isIncreasingOpacity) {
            this.targetOpacity = 255;
        } else {
            this.targetOpacity = 0;
        }

        let originalOpacity = this.sprite.color.a;

        let frames = this.duration * 60;
        let step = (originalOpacity - this.targetOpacity) / frames;

        let counter = 0;

        let transparencyInterval = setInterval(() => {
            if (counter >= frames) {
                clearInterval(transparencyInterval);
                this.isIncreasingOpacity = !this.isIncreasingOpacity;
            } else {
                let nextOpacity = originalOpacity - step * counter;
                this.sprite.color = new Color(255,255,255,Math.round(nextOpacity));
                counter++;
            }
        }, 1000 / 60); // 60 fps
    }
}


