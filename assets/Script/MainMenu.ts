import { _decorator, Component, Node, director, sys, Button, NodeEventType, Animation } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('MainMenu')
export class MainMenu extends Component {

    @property({ type: String })
    sceneName: string = 'SceneGame';

    @property({
        type: [Node]
    }) 
    public btnSkin: Node[] = [];   

    protected onLoad(): void {
        sys.localStorage.setItem("Skin", 0);
    }

    addCustomEvent(){
        this.btnSkin[0].on(Node.EventType.TOUCH_END , () => {
            this.chooseSkin(0);
            this.btnSkin[0].getComponent(Animation).play();
            this.btnSkin[1].getComponent(Animation).stop();
            this.btnSkin[2].getComponent(Animation).stop();
        })
        this.btnSkin[1].on(Node.EventType.TOUCH_END , () => {
            this.chooseSkin(1);
            this.btnSkin[0].getComponent(Animation).stop();
            this.btnSkin[1].getComponent(Animation).play();
            this.btnSkin[2].getComponent(Animation).stop();
        })
        this.btnSkin[2].on(Node.EventType.TOUCH_END , () => {
            this.chooseSkin(2);
            this.btnSkin[0].getComponent(Animation).stop();
            this.btnSkin[1].getComponent(Animation).stop();
            this.btnSkin[2].getComponent(Animation).play();
        })
    }

    openSceneGame(){
        director.loadScene(this.sceneName, () => {
            console.log('Scene Game');
            // todo
        });
    }

    chooseSkin(index: number){
        sys.localStorage.setItem("Skin", index);
        console.log('Skin: ' + index);           
    }
}


